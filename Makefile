SHELL = /bin/sh
CC    = g++

CFLAGS = -W -g -Isrc/h 

TARGET  = ah1a ah1b ah2 ah3 ah4
EXES = $(shell echo src/*.cpp)
INCLUDES = $(shell echo src/include/*.cpp)
OBJECTS = $(EXES:.c=.o,INCLUDES:.c=.o)
 
all: $(TARGET)
 
$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ src/$@.cpp $(INCLUDES)






