#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <string.h>

#define PI 			3.1415926
#define HEIGHT		14
#define WIDTH		30

int main(int argc, char **argv)
{
	if (argc != 3) {
		printf("Usage: %s [INPUT TANK DATA] [INPUT SHADOW DATA]\n", argv[0]);
		return -1;
	}
		

	//open the file with tank data
	FILE * file_t;
	file_t = fopen(argv[1], "r");
	if (file_t == NULL)
	{
		printf("Can't open data file %s\n", argv[1]);
		return -1;
	}
	
	FILE * file_s;
	file_s = fopen(argv[2], "r");
	if (file_t == NULL)
	{
		printf("Can't open data file %s\n", argv[2]);
		return -1;
	}
	
	char rline [256] = {0};	
	
	while (fgets (rline, sizeof(rline), file_t) != NULL)
	{
		char * pch;
		pch = strtok (rline, ",");
		int x = atoi(pch);
		pch = strtok (NULL, ",");
		int y = atoi(pch);
		pch = strtok (NULL, ",");
		int r = atoi(pch);

		fseek(file_s, 0, SEEK_SET);
		char rline2 [256] = {0};	
		bool one_found = false;
		bool two_found = false;
		int o_x, o_y, o_r;
		float o_d;
		
		while (fgets (rline2, sizeof(rline2), file_s) != NULL)
		{
			pch = strtok (rline2, ",");
			int x2 = atoi(pch);
			pch = strtok (NULL, ",");
			int y2 = atoi(pch);
			pch = strtok (NULL, ",");
			int r2 = atoi(pch);
		
			float d = sqrt(pow(x2 - x, 2) + pow(y2 - y, 2));
			if (d < r)
			{
				if (one_found)
				{
					float rt;
					if (d > o_d)
						rt = (HEIGHT - o_d) / d; 
					else 
						rt = (HEIGHT - d) / o_d;
					
					float volume = PI * r * r * HEIGHT * rt;
					printf ("%d,%d,%.2f\n", x, y, volume);
					two_found = true;
					break;
				}
				else
				{
					o_x = x2;
					o_y = y2;
					o_r = r2;
					o_d = d;
					one_found = true;
				}
			}		
		}
		if (!two_found)
			printf ("%d,%d,%.2f\n", x, y, PI * r * r * HEIGHT);

	}
	fclose(file_t);
	fclose(file_s);
	
	return 0;
}