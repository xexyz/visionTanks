#include "Image.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#define PI 			3.1415926
#define ANGLES 		360
#define RADII_MAX	32
#define RADII_MIN	32
#define DINV		1.0
#define BOUND		5


int main(int argc, char **argv)
{
	if (argc != 4) {
		printf("Usage: %s [INPUT BINARY EDGE IMAGE] [INPUT ITEM COUNT] [OUTPUT GRAY-LEVEL CIRCULAR HOUGH IMAGE]\n", argv[0]);
		return -1;
	}
		

	//open the image
	Image * im;
	im = new Image;
	assert(im != 0);
	if (readImage(im, argv[1]) != 0)
	{
		printf("Can't open file %s\n", argv[1]);
		return -1;
	}
	int rows = im->getNRows() / DINV;
	int cols = im->getNCols() / DINV;
	int sx[RADII_MAX - RADII_MIN + 1][rows][cols];
	for (int i = 0; i < rows ; i++)	
		for (int j = 0; j < cols; j++)		
				for (int l = RADII_MIN; l <= RADII_MAX; l++)
					sx[l - RADII_MIN][i][j] = 0;
					
	//main loop
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			if (im->getPixel(i, j) == 0)
				continue;
				
			for (int k = 0; k < ANGLES; k++)
			{
				for (int l = RADII_MIN; l <= RADII_MAX; l++)
				{
					float a = i - l * cos(k * PI/ 180);
					float b = j - l * sin(k * PI/ 180);
					int a_i = (int)floor ((a * (1 / DINV)) + 0.5) / (1 / DINV);
					int b_i = (int)floor ((b * (1 / DINV)) + 0.5) / (1 / DINV);
					if (a_i < 0
						|| a_i >= rows 
						|| b_i < 0
						|| b_i >= cols)
						continue;
					sx[l - RADII_MIN][a_i][b_i]++;
				}
			}
		}
	}	
	
	//find the maximum values.
	int obj = atoi(argv[2]);
	int obj_x[obj + 1];
	int obj_y[obj + 1];
	int obj_r[obj + 1];
	
	for (int o = 0; o < obj; o++)
	{
		int val = 0;
		int x, y, r;

		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				for (int l = RADII_MIN; l <= RADII_MAX; l++)
				{
					if (sx[l - RADII_MIN][i][j] > val)
					{
						for (int k = 0; k < i; k++)
						{
							if (i > obj_x[k] - BOUND
								&& i < obj_x[k] + BOUND
								&& j > obj_y[k] - BOUND
								&& j < obj_y[k] + BOUND)
							goto esc;
						}
						x = i;
						y = j;
						r = l;
						val = sx[l - RADII_MIN][i][j];
					}
					esc:
					continue;
				}
			}	
		}
		printf("%d,%d,%d\n", x, y, r);
		obj_x[o] = x;
		obj_y[o] = y;
		obj_r[o] = r;
	}
	
	//output image
	Image * oim;
	oim = new Image;
	assert(oim != 0);
	oim->setSize(rows, cols);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			oim->setPixel(i, j, sx[obj_r[0] - RADII_MIN][i][j]);

	if (oim->setColors(254) != 254)
	{
		printf("Can't set colors\n");
		return -1;
	}
	if (writeImage(oim, argv[3]) != 0)
	{
		printf("Can't write to file %s\n", argv[3]);
		return -1;
	}
	
	return 0;
}