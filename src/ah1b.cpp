#include "Image.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main(int argc, char **argv)
{
	if (argc != 4) {
		printf("Usage: %s [INPUT GRAY-LEVEL IMAGE] [INPUT GRAY-LEVEL THRESHOLD] [OUTPUT BINARY IMAGE]\n", argv[0]);
		return -1;
	}
	
	int thres = atoi(argv[2]);
	
	Image * im;
	im = new Image;
	assert(im != 0);
	if (readImage(im, argv[1]) != 0)
	{
		printf("Can't open file %s\n", argv[1]);
		return -1;
	}
	
	for (int i = 0; i < im->getNRows(); i++)
	{
		for (int j = 0; j < im->getNCols(); j++)
		{
			if (im->getPixel(i, j) > thres)
				im->setPixel(i, j, 0);
			else
				im->setPixel(i, j, 255);
		}
	}

	if (im->setColors(1) != 1)
	{
		printf("Can't set colors\n");
		return -1;
	}
	if (writeImage(im, argv[3]) != 0)
	{
		printf("Can't write to file %s\n", argv[3]);
		return -1;
	}
	
	return 0;
}
