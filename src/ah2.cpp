#include "Image.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main(int argc, char **argv)
{
	if (argc != 3) {
		printf("Usage: %s [INPUT GRAY-LEVEL IMAGE] [OUTPUT GRAY-LEVEL EDGE IMAGE]\n", argv[0]);
		return -1;
	}
	
	//the Sobel Mask
	int sx[3][3];
	sx[0][0] = -1; sx[0][1] = 0; sx[0][2] = 1;
    sx[1][0] = -2; sx[1][1] = 0; sx[1][2] = 2;
	sx[2][0] = -1; sx[2][1] = 0; sx[2][2] = 1;
	
	int sy[3][3];
	sy[0][0] =  1; sy[0][1] =  2; sy[0][2] =  1;
    sy[1][0] =  0; sy[1][1] =  0; sy[1][2] =  0;
    sy[2][0] = -1; sy[2][1] = -2; sy[2][2] = -1;
	
	//open the image
	Image * im;
	im = new Image;
	assert(im != 0);
	if (readImage(im, argv[1]) != 0)
	{
		printf("Can't open file %s\n", argv[1]);
		return -1;
	}
	
	//output image
	Image * oim;
	oim = new Image;
	assert(oim != 0);
	oim->setSize(im->getNRows(), im->getNCols());
	
	//main loop
	for (int i = 0; i < im->getNRows(); i++)
	{
		for (int j = 0; j < im->getNCols(); j++)
		{
			long x_sum = 0;
			long y_sum = 0;
			int value = 0;
			
			if (i != 0 
				&& j != 0
				&& i != im->getNRows() - 1
				&& j != im->getNCols() - 1) //boundaries
			{
				for (int k = -1; k <= 1; k++)
					for (int m = -1; m <= 1; m++)
						x_sum += im->getPixel(i + k, j + m) * sx[k + 1][m + 1];
					
				for(int k = -1; k <= 1; k++)
					for (int m = -1; m <= 1; m++)
						y_sum += im->getPixel(i + k, j + m) * sy[k + 1][m + 1];
			
				value = abs(x_sum) + abs(y_sum);
			}			
			
			if (value > 255)
				value = 255;
			if (value < 0)
				value = 0;
			oim->setPixel(i, j, value);
		}
	}

	if (oim->setColors(255) != 255)
	{
		printf("Can't set colors\n");
		return -1;
	}
	if (writeImage(oim, argv[2]) != 0)
	{
		printf("Can't write to file %s\n", argv[2]);
		return -1;
	}
	
	return 0;
}
